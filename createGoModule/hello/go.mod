module createGoModule/hello

go 1.16

replace createGoModule/greetings => ../greetings

require createGoModule/greetings v0.0.0-00010101000000-000000000000
